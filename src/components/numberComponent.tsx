import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux"
import { incrementNumber, decrementNumber } from "../action/numberAction";

export default function NumberComponent() {
    const count = useSelector((state: any) => state.numberReducer.number);
    const dispatch = useDispatch();
    function increment() {
        dispatch(incrementNumber(5))
    }

    function decrement() {
        dispatch(decrementNumber(2))
    }
    
    return (
        <div>
            <h1>{count}</h1>
            <button onClick={increment} >Increment</button>
            <button onClick={decrement} >Decrement</button>

        </div>
    );

}