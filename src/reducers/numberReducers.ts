import { INCREMENT_NUMBER, DECREMENT_NUMBER } from '../action/actionTypes';

const initialState = {
  number: 20,
};
export const numberReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case INCREMENT_NUMBER:
      return {
        ...state,
        number: state.number + action.payload,
      };
    case DECREMENT_NUMBER:
      return {
        ...state,
        number: state.number - action.payload,
      };
    default:
      return state;
  }
};
