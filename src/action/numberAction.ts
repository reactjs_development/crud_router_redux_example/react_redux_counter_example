import {INCREMENT_NUMBER,DECREMENT_NUMBER} from './actionTypes'

export const incrementNumber = ( num:number) => {
return {
    type : INCREMENT_NUMBER,
    payload :num
}

}

export const decrementNumber = ( num:number) => {
    return {
        type : DECREMENT_NUMBER,
        payload :num
    }
    
    }