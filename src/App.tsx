import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import IncrementDecrement from './components/numberComponent';
const state = configureStore({});
function App() {
  return (
    <Provider store={state}>
      <div className="App">
        <IncrementDecrement />
      </div>

    </Provider>
  );
}

export default App;
